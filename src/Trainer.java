import java.util.ArrayList;

/**
 * Created by Dmitry Vereykin on 12/1/2015.
 */


public class Trainer {
    public ArrayList<Game> games = new ArrayList<Game>();
    public String str = "";

    public void play() {
        int indexOfMove;
        int indexOfGame = 0;

            while (true) {
                str += "\n----------------------------------------------------------------------------\n";
                str += "Game " + (indexOfGame + 1) + "\n";
                Game game = new Game();
                indexOfMove = 0;
                Move tempMove;

                //First game (it is different because there is no previous to check)
                if (indexOfGame == 0) {
                    tempMove = game.initialMove();
                    game.moves.add(tempMove);
                    indexOfMove++;
                    str += indexOfMove + ". " + tempMove + "\n";

                    while (true) {
                        tempMove = game.move(tempMove);
                        game.moves.add(tempMove);
                        indexOfMove++;
                        str += indexOfMove + ". " + tempMove + "\n";

                        if (game.checkWin(tempMove))
                            break;
                    }

                    games.add(game);

                //Starting from 2nd game
                } else {

                    tempMove = game.initialMove();

                    while (true) {
                        if (findPrevious(tempMove)) {
                            game.moves.add(tempMove);
                            indexOfMove++;
                            str += indexOfMove + ". " + tempMove + "\n";
                            break;
                        } else {
                            tempMove = game.initialMove();
                        }
                    }

                    while (true) {

                        tempMove = game.move(tempMove);

                        while (true) {
                            if (findPrevious(tempMove)) {
                                game.moves.add(tempMove);
                                indexOfMove++;
                                str += indexOfMove + ". " + tempMove + "\n";
                                break;
                            } else {
                                tempMove = game.move(tempMove);
                            }
                        }

                        if (game.checkWin(tempMove))
                            break;
                    }

                    games.add(game);
                }

                if (games.get(indexOfGame).moves.size() <= 5) {
                    str += "\nIdeal solution was found! Good job computer!" + "\n";
                    break;
                }

                indexOfGame++;

            }


    }

    //This method checks previous games (starting at 2nd game)
    public boolean findPrevious(Move comparedMove) {
        boolean flag = true;

        for (int indexOfGame = 0; indexOfGame < games.size(); indexOfGame++) {
            for (int indexOfMove = 0; indexOfMove < games.get(indexOfGame).moves.size(); indexOfMove++) {
                if ((games.get(indexOfGame).moves.get(indexOfMove).getInitialState().getCoin(0).equals(comparedMove.getInitialState().getCoin(0))) && //found and legal
                        (games.get(indexOfGame).moves.get(indexOfMove).getInitialState().getCoin(1).equals(comparedMove.getInitialState().getCoin(1))) &&
                        (games.get(indexOfGame).moves.get(indexOfMove).getInitialState().getCoin(2).equals(comparedMove.getInitialState().getCoin(2))) &&
                        (games.get(indexOfGame).moves.get(indexOfMove).getFinalState().getCoin(0).equals(comparedMove.getFinalState().getCoin(0))) &&
                        (games.get(indexOfGame).moves.get(indexOfMove).getFinalState().getCoin(1).equals(comparedMove.getFinalState().getCoin(1))) &&
                        (games.get(indexOfGame).moves.get(indexOfMove).getFinalState().getCoin(2).equals(comparedMove.getFinalState().getCoin(2))) &&
                        (games.get(indexOfGame).moves.get(indexOfMove).getLegalMove())) {
                    return true;
                } else if ((games.get(indexOfGame).moves.get(indexOfMove).getInitialState().getCoin(0).equals(comparedMove.getInitialState().getCoin(0))) && //found and illegal
                        (games.get(indexOfGame).moves.get(indexOfMove).getInitialState().getCoin(1).equals(comparedMove.getInitialState().getCoin(1))) &&
                        (games.get(indexOfGame).moves.get(indexOfMove).getInitialState().getCoin(2).equals(comparedMove.getInitialState().getCoin(2))) &&
                        (games.get(indexOfGame).moves.get(indexOfMove).getFinalState().getCoin(0).equals(comparedMove.getFinalState().getCoin(0))) &&
                        (games.get(indexOfGame).moves.get(indexOfMove).getFinalState().getCoin(1).equals(comparedMove.getFinalState().getCoin(1))) &&
                        (games.get(indexOfGame).moves.get(indexOfMove).getFinalState().getCoin(2).equals(comparedMove.getFinalState().getCoin(2))) &&
                        (!games.get(indexOfGame).moves.get(indexOfMove).getLegalMove())) {
                    str += "                                                    " + games.get(indexOfGame).moves.get(indexOfMove) + " : was learned already."  + "\n";
                    return false;

                }
            }
        }

        return flag; //If move wasn't found it returns true that makes Trainer to add it as a new move
    }

    //Returns string to GUI's TextArea
    public String getStr() {
        return str;
    }
}



