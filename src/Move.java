public class Move {
    public State initialState;
    public State finalState;
    public int flips;
    public boolean legalMove;

    Move() {
        initialState = new State();
        finalState = new State();
    }

    Move(State stateFromPrev) {
        initialState = new State();
        initialState.setCoin(0, stateFromPrev.getCoin(0));
        initialState.setCoin(1, stateFromPrev.getCoin(1));
        initialState.setCoin(2, stateFromPrev.getCoin(2));
        finalState = new State();
        finalState.setCoin(0, stateFromPrev.getCoin(0));
        finalState.setCoin(1, stateFromPrev.getCoin(1));
        finalState.setCoin(2, stateFromPrev.getCoin(2));
    }

    Move(String coin0, String coin1, String coin2) {
        initialState = new State(coin0, coin1, coin2);
        finalState = new State(coin0, coin1, coin2);
    }

    public int getFlips() {
        return flips;
    }

    public void setLegalMove(boolean legalMove) {
        this.legalMove = legalMove;
    }

    public boolean getLegalMove() {
        return this.legalMove;
    }

    public State getFinalState() {
        return finalState;
    }

    public State getInitialState() {
        return initialState;
    }

    public void flip(int index) {
        flips++;
        if (initialState.getCoin(index).equals("H"))
            finalState.setCoin(index, "T");
        else
            finalState.setCoin(index, "H");
    }

    public String toString() {
        String legality;

        if (legalMove) {
            legality = "Legal";
        } else {
            legality = "Illegal";
        }

        return initialState.getCoin(0) + " " + initialState.getCoin(1) + " " + initialState.getCoin(2) + " --> "
                + finalState.getCoin(0) + " " + finalState.getCoin(1) + " " + finalState.getCoin(2) + " : " + legality;
    }

}
